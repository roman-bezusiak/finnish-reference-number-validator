# Finnish Reference Number Validator

Validator for Finnish reference numbers.

## Operation

It checks, whether a finnish reference number:

1. Contains a properly calculated check number
2. Is properly intended (groups of 5 digits from right to left with spaces inbetween)
3. Doesn't start with a space
4. Doesn't contain anything except for spaces and digits
