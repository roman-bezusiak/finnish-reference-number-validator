package fi.vamk.e1700684;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import fi.vamk.e1700684.ReferenceValidator;

/**
 * Unit test for simple App.
 */
public class ReferenceValidatorTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void isFinnishInvoiceReferenceNumberValidTest()
    {
        // ---- Invalid cases ----
        assertFalse(ReferenceValidator.isFinnishInvoiceReferenceNumberValid("114"));
        assertFalse(ReferenceValidator.isFinnishInvoiceReferenceNumberValid("1 23456 78901 23456 78901"));
        assertFalse(ReferenceValidator.isFinnishInvoiceReferenceNumberValid("1 1111d"));
        assertFalse(ReferenceValidator.isFinnishInvoiceReferenceNumberValid(" 11111 11117"));
        
        // ---- Valid case ----
        assertTrue(ReferenceValidator.isFinnishInvoiceReferenceNumberValid("11111 11117"));
    }
}
