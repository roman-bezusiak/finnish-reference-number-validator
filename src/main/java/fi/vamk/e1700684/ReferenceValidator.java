package fi.vamk.e1700684;

/**
 * Finnish invoice reference number validator
 *
 * This class has only one method. It is used for Finnish invoice
 * reference number string validation. 
 * 
 * @author Roman Bezusiak
 */
public class ReferenceValidator 
{
    /**
     * Returns a boolean representing whether given Finnish invoice
     * reference number (FIRN) string is valid or not.
     * 
     * Estimation criteria:
     * 
     *     1. Space character disposition (anywhere in the string)
     *     2. Check number validation
     *     3. Length validation
     * 
     * FIRN examle: "nn nnnnn nnnnc", where 'n' in part of the invoice
     * number, 'c' - check number, and ' ' (space) is the delimiter
     * between 5 consecutive digits in the number (starting from the right).
     * 
     * Example of check number calculation:
     * 
     *                      FIRN: "11111 11117"
     *     Per-digit multipliers:  13713 7137
     *     
     *     1. Per-digit multiplication results:  13713 7137
     *     2. Per-digit multiplication result sum: 1+3+7+1+3+7+1+3+7=33
     *     3. Next full 10 of per-digit multiplication result sum: 33 -> 40
     *     4. Check number (difference of previous 2 steps): 40 - 33 = 7
     * 
     * @author Roman Bezusiak
     * @param invoiceReferenceNumber FIRN string to be validated
     * @return true -> FIRN is valid, false -> FIRN is invalid
     */
    public static boolean isFinnishInvoiceReferenceNumberValid(
        String invoiceReferenceNumber)
    {
        int irnl = invoiceReferenceNumber.length(); // Expected max: 23

        if (irnl >= 4 && // Min length check (3 (invoice) + 1 (check))
            irnl % 6 != 0 && // If false, the number starts with a space
            irnl <= 23) // Max length check (19 (invoice) + 1 (check) + 3 (spaces))
        {
            byte inl = (byte) (irnl - 1); // Invoice number length (without the check number at the end)
            byte spaces = 0; // Amount of spaces encountered in parsing. Max: 3 (decimal)
            char c = ' '; // Char buffer
            boolean isSpace = false; // State of whether char buffer 'c' contains a space character
            boolean isSpaceLocation = false; // State of whether a space should be at current position

            short calculationBuffer = 0; // Max: 630 (decimal)
            byte testedCheckingNumber = 0; // Max: 9 (decimal)

            try
            {
                for (byte i = 0; i < inl; i++)
                {
                    c = invoiceReferenceNumber.charAt(i);
                    isSpace = c == ' ';
                    isSpaceLocation = irnl > 5 && (irnl - i) % 6 == 0;

                    if (!isSpace)
                    {
                        if (isSpaceLocation) return false; // No space where needed
                        
                        switch ((i - spaces) % 3)
                        {
                            case 0:
                                calculationBuffer += (short) Byte.parseByte(String.valueOf(c));
                                break;
                            case 1:
                                calculationBuffer += (short) (3 * Byte.parseByte(String.valueOf(c)));
                                break;
                            default:
                                calculationBuffer += (short) (7 * Byte.parseByte(String.valueOf(c)));
                        }
                    }
                    else
                    {
                        if (!isSpaceLocation) return false; // Space where not needed
                        spaces++;
                    }
                }

                // Parsing the rightmost char (tested checking number)
                testedCheckingNumber = Byte.parseByte(String.valueOf(
                    invoiceReferenceNumber.charAt(inl)
                ));
            }
            catch (Exception e)
            {
                return false;
            }

            return testedCheckingNumber == 10 - calculationBuffer % 10;
        }
        else
        {
            return false;
        }
    }
}
